# TBBlue

Formato do SD: FAT16 ou FAT32, porém o sistema operacional divMMC ainda não suporta nomes longos de arquivo.

Descompactar o arquivo "SD Card.rar" na raiz do cartão SD, após a formatação.

A máquinas sintetizadas são ZX Spectrum e compatíveis.

- ZX Spectrum 48Kb
- ZX Spectrum 128kb
- ZX Spectrum +3
- TK90X/TK95

Emuladas
- ZX80
- ZX81/TK85

#### Bugs conhecidos
- Durante o carregamento do ESXDOS é normal aparecer a mensagem "RTC.SYS error", já que se trata de uma máquina sintetizada sem o Real Time Clock.

##### Change log

- 1.07.1: 07/03/2017 - novo controlador de teclado PS2. Agradecimento a Edson Kadoya pelo report do bug e testes.
- 1.07 : 26/11/2016 - versão inicial