# Arcades

Algumas máquinas tem orientação vertical.

##### Bugs conhecidos

O hardware que gera as estrelas de fundo do Galaxian não esta corretamente sintetizado.

Super Glob parece que usa um botão de ação, que não está sintetizado no hardware.


##### Change log

- 003 - 30/07/2019 - Jogo River Raid em FPGA. Convertido para o MC por Mauricio Melo.

- 002 - 24/12/2016 - hardware do Pac-man atualizado e ampliado. Incluido Roller Crusher, Gorkans, Mr TNT, Ms Pac-Man e Super Glob. Versão inicial do hardware da Galaxian.

- 001 - 24/11/2016 - versão inicial Pac-Man, Scramble e Frogger