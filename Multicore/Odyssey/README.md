# Odyssey

Formato do SD: FAT16 ou FAT32, com suporte a nomes longos de arquivo.
Reset: Botão 3 + Botão 4 volta à tela do SD Loader

Scanlines opcionais: Botão 1 + Botão 2 ativam/desativam scanlines


Arquivos JIC e SOF, de acordo com a versão, VGA ou HDMI.


##### Bugs conhecidos

- Quando o SD Loader carrega o cartão pela primeira vez o conteúdo não costuma ser exibido. Basta um Reset.

- Super Bee : Na tela de game over, as patas da aranha parecem desenhadas incorretamente


##### Change log

- 003 : 12/03/2018 - versão Multicore 2
- 002 : 25/11/2016 - Scanlines opcionais incluidas
- 001 : 24/11/2016 - versão inicial