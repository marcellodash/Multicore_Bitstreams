# TRS-80 Model III

Formato do SD: Sem suporte a SD no momento. Os arquivos podem ser lidos pela entrada auxiliar do Multicore.

Entrada Auxiliar: Plug P2 estéreo, ponta do plug é a saída MIC para gravação de dados, conexão do meio é o EAR para a leitura e a carcaça é o terra.

Reset: Botão 3 + Botão 4 ou F4 do teclado.

Scanlines opcionais: Botão 1 + Botão 2 ativam/desativam scanlines.

A máquina sintetizada é um TRS-80 Model III utilizando a ROM do CP-300 nacional.

Correlação das teclas do TRS-80 e do teclado PS/2:

    TRS80   PS/2
    CLEAR   ESC ou HOME
    BREAK   DELETE



##### Change log

- 002 : 28/03/2017 - Adicionado scanlines opcionais
- 001 : 27/03/2017 - versão inicial