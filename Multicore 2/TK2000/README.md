# TK2000

- VGA
- Drive DiskII

Formato do SD: RAW, formato próprio.

Verifique a pasta "/Utils" para maiores informações sobre a imagem dos discos para o cartão SD

#### Uso

Colocar o arquivo "TK2000.MC2" na raiz de um cartão SD para o boot. Após iniciado, trocar o SD para o cartão em formato RAW.

##### Change log

- 002 : 09/07/2018 - versão inicial Multicore 2
- 001 : 16/03/2017 - versão inicial