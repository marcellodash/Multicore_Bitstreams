# Commodore 64

- VGA
- PAL (50Hz) / NTSC (60HZ)
- Drive 1541
- suporte ao CI de som SID
- suporte a 2 joysticks

Formato do SD: RAW, formato próprio.

Hard Reset: Botão 3 + Botão 4

Scanlines opcionais: Botão 1 + Botão 2 ativam/desativam scanlines

Inverter portas dos joysticks : Botão 1 ou F11 no teclado

PAL ou NTSC : F12 no teclado

O SD é lido como um repositórios de imagens D64 (imagens de 174 848 bytes). Cada imagem deve ficar em limites de 256K.
Então, ao montar o arquivo de imagem, temos em 0x00000 o primeiro D64, 0x40000 para o segundo, 0x80000 o terceiro, 0xC0000 o quarto, 0x100000 o quinto, 0x140000 o sexto e assim seguindo o mesmo padrão para todas as próximas imagens.

O arquivo final, com todas as imagens concatenadas pode ser transferido para o cartão pelo programa Win32DiskImage ou pelo "dd" no Linux/MacOS.

#### Uso

Colocar o arquivo "C64.MC2" na raiz de um cartão SD para o boot. Após iniciado, trocar o SD para o cartão em formato RAW.

##### Change log

- 003 : 09/07/2018 - versão inicial Multicore 2
- 002 : 19/12/2016 - melhorado as cores com scanlines
- 001 : 13/12/2016 - versão inicial