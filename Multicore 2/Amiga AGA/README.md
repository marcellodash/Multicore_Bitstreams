# Amiga AGA

Microcomputador Amiga AGA (exemplo Amiga 1200) para o Multicore 2.

Copiar os arquivos "AmigaAGA.MC2", "Kick.ROM" e "de1_boot.bin" para a raiz do cartão SD.

Utilize a tecla F12 para usar o menu e carregar os jogos. Os arquivos tem a extensão ADF.

O joystick deve ser utilizado na porta de Joystick 2 (DB9 direito no Multicore)

Requer Teclado (porta PS/2 erquerda) e Mouse (porta PS/2 direita)

##### Change log

- 001 : 30/09/2018 - Versão inicial