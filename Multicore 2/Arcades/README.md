# Arcades

Copie o arquivo ".MC2" da máquina desejada para a raiz de um cartão SD, formatado em FAT ou FAT32.

##### Observações

Alguns hardwares são necessários dois arquivos. Descompacte o ZIP na raiz do cartão SD.

##### Bugs conhecidos

Algumas máquinas tem orientação vertical.

O hardware que gera as estrelas de fundo do Galaxian não esta corretamente sintetizado.

Super Glob parece que usa um botão de ação, que não está sintetizado no hardware.

##### Change log

- 005 - 23/07/2019 - Xevious e Time Pilot finais, com novos loaders e saida HDMI

- 004 - 01/11/2018 - update hardware do Time Pilot

- 003 - 12/04/2018 - hardware do Time Pilot

- 002 - 24/12/2016 - hardware do Pac-man atualizado e ampliado. Incluido Roller Crusher, Gorkans, Mr TNT, Ms Pac-Man e Super Glob. Versão inicial do hardware da Galaxian.

- 001 - 24/11/2016 - versão inicial Pac-Man, Scramble e Frogger